#!/bin/sh

entrypoint_log() {
    if [ -z "${NGINX_ENTRYPOINT_QUIET_LOGS:-}" ]; then
        echo "$@"
    fi
}

MONITOR_FOLDER=${MONITOR_FOLDER:-"/etc/nginx/conf.d"}
UPDATE_INTERVAL=${UPDATE_INTERVAL:-5}

get_folder_hash() {
    HASH_SUM="$(sha1sum ${MONITOR_FOLDER}/*.conf | sha1sum)"
    echo $HASH_SUM
}

FOLDER_HASH="$(get_folder_hash)"

configuration_monitor() {    
    entrypoint_log "$0: Start 'configuration_monitor'!";
    entrypoint_log "$0: Path to monitor folder: $MONITOR_FOLDER";
    entrypoint_log "$0: Update interval: $UPDATE_INTERVAL sec";
    while true
    do
        NEW_FOLDER_HASH="$(get_folder_hash)"

        if [ "$NEW_FOLDER_HASH" != "$FOLDER_HASH" ]; then
            FOLDER_HASH="$NEW_FOLDER_HASH"
            nginx -s reload
        fi
        sleep $UPDATE_INTERVAL
    done
}

configuration_monitor &

sleep 1
